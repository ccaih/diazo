from zope.interface import Interface
from plone.app.z3cform.interfaces import IPloneFormLayer
from plonetheme.sunburst.browser.interfaces import IThemeSpecific as ISunburstLayer
from collective.geo.mapwidget.browser.interfaces import IGeoMapWidgetLayer

class ITerranovaLayer(ISunburstLayer, IGeoMapWidgetLayer, IPloneFormLayer):
    """Marker interface that defines a Zope 3 browser layer.
    """
