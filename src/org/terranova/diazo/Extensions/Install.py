# GenericSetup still relies on uninstall method to execute uninstall steps.
# Here we just call the uninstall profile of this package.

from Products.CMFCore.utils import getToolByName

def uninstall(portal):
    setup_tool = getToolByName(portal, 'portal_setup')
    setup_tool.runAllImportStepsFromProfile('profile-org.terranova.diazo:uninstall')
    return "Ran all uninstall steps."
