jQuery(document).ready(function($) {
    //make the selects chose and search if more than 10 items
    // $("select").chosen({ disable_search_threshold: 10 });
      'use strict';
    $('.dropdown-toggle').dropdown();

    //add the selected class to the nav item based on page

    $(".nav li:has(a[href$='"+window.location.pathname+"'])").addClass("active");
    //close the search parameters box if none selected searchparameters
    $("#searchparameters:not(:has(li))").collapse("hide");
    //add the plus sign for accordion headers only if they have a child nav
       //set up the accordion for the facet drop downs
       $(".facets .collapse").collapse();
       $(".facetmenu li").parent().parent().parent().prev(".nav-item-header")
       .prepend("&nbsp;<i class='icon-chevron-down icon-grey'></i>&nbsp;")
       .on("click",function () {
        $(this).next(".collapse").collapse('toggle');
        $("i", this).toggleClass("icon-chevron-down icon-play");
      })
       .on("hover",function(){
        $(this).css('cursor', 'pointer');
      });
        //change the more label

        $("h4 .label").toggle(function() {
          $(this).text('- Less');
        }, function() {
          $(this).text('+ More');
        }).on("hover",function(){
          $(this).css('cursor', 'pointer');
        }).on("click",function(){

          $(this).parent().prev(".collapse").collapse('toggle');

        }).parent().prev(".collapse").attr("style", "height:0");


 //add the text of the first span to the title of the link to show on hover in the facets

     $('.filterchoice').each(function () {
       $(this).attr("title", $(':nth-child(1)', $(this)).html());
               //if facet is empty, add text 'untitled'
               if ($(':nth-child(1)', $(this)).html() === ""){
                $(':nth-child(1)', $(this)).html("untitled");
              }
        });


    //replace the text filed under with the tags icon
    if($("#searchtags").length){
    $("#searchtags").html($("#searchtags").html().replace("Filed under:", "<i class='icon-tags icon-grey'> </i>"));
    }

    $('.chosen').chosen({ disable_search_threshold: 10 });
  //  $( '.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });

 



  //load the regions based on the chosen state from json
  var stateChoice;
  $('#stateselect').chosen().change(function () {
      stateChoice = $('#stateselect option:selected').val();
      //$('select option:selected').trigger('liszt:updated');
      console.log(stateChoice);
      loadJson(stateChoice);
    });

  var loadJson = function (state) {
      $('#regions p').html('<label>Local Government Area:</label> <select class=selectorLGA></select><hr><label>Natural Resource Management Region:</label> <select class=selectorNRM></select><hr><label>Interim Biogeographic Regionalisation for Australia Region:</label> <select class=selectorIBRA></select>');
      $('#regions select').addClass('chzn-select');
      $.getJSON('data/' + state + '.json', function(data) {
        $.each(data.result.places, function(entryIndex, entry) {
          $('.selector' + entry.name).html('');
          var options;
          $('select').attr('data-placeholder','choose an area..');
          $.each(entry.attributes, function(attIndex, att) {
            options += '<option> ' + att + '</option>';
          });
          $('.selector' + entry.name).append(options);
        });
            //make the regions chosen
            $('.chzn-select').chosen({ disable_search_threshold: 10 });
            //revert any changed selectors so only one selector can be used
            //find the current select and revert the others
            $('.chzn-select').chosen().change(function(){
          var currentClass = $(this).attr('class').split(' ');
            $('select:not(.' + currentClass[0] + ',.stateselect)').val('').trigger('liszt:updated');
            $('#stateselect option:selected').val(stateChoice);
            console.log(stateChoice);

            });
        });
      return false;
    };

    //add to my collection

    $("a[data-toggle=popover]").click(function(e){
      $('.popover').hide();
      var el=$(this);
      $.get(el.attr('data-load'),function(d){
        el.popover({
          title:"<span class='testlink'> Add to collection</span><a class='popover-close'></a>",
          trigger:'manual',
          html:true,
          content: function() {
          return "Please choose collection<br><hr>" + d ;
          }}).popover('show');
       });
      e.preventDefault();
    });

    $(document).on("click", ".popover-close", function(e){
      e.preventDefault();
      $('.popover').hide();

    });

    //Upload Form
    //grab the titles from the formTabs select and build the tabs

    // var formTabs = [];
    // $(".formTabs option").each(function(){
    //   formTabs.push($(this).text());
    // });


    // //find the ids of the fieldsets i.e all with .formPanel
    // var panelIDs = [];
    // $(".formPanel").each(function(){
    //   panelIDs.push($(this).attr("id"));
    // });

    // //build the tabs
    // var tabHTML = "<div class='arrow-left'></div>";
    // $.each(formTabs, function(m,k){
    //            tabHTML += "<div class='tab-up atab'><a href='#" + panelIDs[m] + "' class='expander' data-toggle='tab'>" + k + "</a></div>";
    //   });
    //<div class='progress progress-success'><div class='bar' id='barsection" + m + "'></div></div>
    //$("#newFormTabs").html(tabHTML);

    //remove the style attribute on the form panels, add inactive class
    $(".formPanel").removeAttr("style").addClass("inactive");

    //make the first panel visible and the first tab active and the title the title of the first tab
    $('.formPanel:first').addClass('active');
    $('.atab:first').removeClass('tab-up').addClass('tab-down');
    $("#uploadForm #itemtitle").html($('.atab:first').text());


    //next button for tabs on form
    //add the bootstrap success btn class to save buttons and inverse to cancel
    $("#form-buttons-save").addClass("btn btn-success").after("<button class='btn btn-primary' id='nexttab' type='button'>Next</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
    $("#form-buttons-cancel").addClass("btn");

    $(document).on('click', '#prevtab', function() {
        $('.arrow-left').remove();
        $('<span class=arrow-left></span>').insertBefore($('.tab-down').prevAll(".tab-up").first());
        $('.tab-down').prevAll(".tab-up").first().find('a[data-toggle="tab"]').tab('show');
        $('.tab-down').prevAll(".tab-up").first().removeClass("tab-up").addClass("tab-down").next('div').removeClass("tab-down").addClass("tab-up");

    });

    $(document).on('click', '#nexttab', function() {
        $('.arrow-left').remove();
        $('<span class=arrow-left></span>').insertBefore($('.tab-down').nextAll(".tab-up").first());
        $('.tab-down').nextAll(".tab-up").first().find('a[data-toggle="tab"]').tab('show');
        $('.tab-down').nextAll(".tab-up").first().removeClass("tab-up").addClass("tab-down").prev().prev().removeClass("tab-down").addClass("tab-up");
        if ($(".atab:last").hasClass('tab-down')){
          $(this).hide();
        }
        $("#uploadForm #itemtitle").html($('.tab-down').text());
    });


 //click on the nav item scrolls to form section and opens panel
  $('.atab').on('click', function (e) {
      $('.atab').removeClass('active').removeClass('tab-down').addClass('tab-up');
      $('.arrow-left').remove();
      $(this).removeClass('tab-up').addClass('tab-down').removeClass('tabHover');
      $('<span class=arrow-left></span>').insertBefore($(this));
       $(this).addClass('active');
     scrollToAnchor();
      //change the title to the tab clicked on
      $("#itemtitle").html($(this).text());
      //hide the next button if this is the last div
      if ($(".atab:last").hasClass('tab-down')){
          $("#nexttab").hide();
        } else {
          $("#nexttab").show();
        }
    });

  function scrollToAnchor(a){
      var aTag = $('a[name=topAnchor]');
      $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }


    //hide the subforms and expand on click
  // $('.subform').hide();
  // $('.addnew').on('click',function () {
  //     $(this).parent().next().next('.subform').toggle('slow');
  //   }).on('hover',function () {
  //     $(this).css('cursor', 'pointer');
  //   });


    //change the error alert to the bootstrap stylee
    $(".error").addClass("alert").addClass("alert-error").removeClass("error").removeClass("portalMessage");

    //hover class for tabs
    $(document).on("mouseover", ".tab-up", function() {
          $(this).addClass("tabHover");
    }).on("mouseout", ".tab-up, .tab-down", function() {
          $(this).removeClass("tabHover");
    });

    $(document).find('#portlets-below .portlet.portletRss .portletItem .portletItemDetails').each(function(){
        $(this).parent().prepend($(this));
    });
//find all the fields in a fieldset that have values, calc as a percentage of total fields   and make div on LHS that length
//find if unsaved changes by adding saved class to all fields and removing it if content changes

  // $('.sectionbutton').on('click',function () {
  //   var btnID = $(this).attr('id');
  //   var sectionClass = btnID.slice(3);
  //   var sectionasClass = $('.' + sectionClass + '');
  //   sectionasClass.addClass('saved');
  //   //find all empty fields
  //   var vEmptyVal = sectionasClass.filter(function () {
  //       return $.trim($(this).val()) !== '';
  //   }).length;
  //   var totalSection = sectionasClass.length;
  //   var sectionpercent = (vEmptyVal/totalSection) * 100 + '%';
  //   $('#bar' + sectionClass + '').css('width', sectionpercent);
  //   //remove the * from the title
  //   $('#bar' + sectionClass + '').parent().parent().find('a').find('span').remove();
  //   $('#' + sectionClass.slice(7) + '').find('h4').find('span').remove();
  //   //make this panel go green then fade out
  //   $(this).parent().parent().effect('highlight', {color:'#90EE90'}, 1000);
  // });

  //remove the saved class of all fields in this fieldset if val is changed

  // $('input,select,textarea').change(function(){
  //   //get this section class
  //   var fieldClass = $(this).attr('class').split(' ');
  //   var classNumber  = fieldClass[1];
  //   $('.' + classNumber +'').removeClass('saved');
  //   //add * next to lh title link - remove 'section' from the class and find the right 'a' from the id of the bar, only if no * already
  //   var barParent = $('#bar' + classNumber + '').parent().parent();
  //   if (barParent.html().indexOf('!') === -1){
  //     barParent.find('a').append('<span class=unsaved> (!)</span>');
  //   }

  // });
    // Modal to choose which container to 'share' to.
    $('body').append('<div id="container-selection" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
                      +'<div class="modal-header">'
                        +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
                        +'<h3 id="myModalLabel">Select Collection</h3>'
                      +'</div>'
                      +'<div class="modal-body">'
                        +'<p><strong>OR</strong> Select another collection within the Terranova repository.</p>'
                        +'<div class="containers"></div>'
                      +'</div>'
                      +'<div class="modal-footer">'
                        +'<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>'
                      //  +'<button class="btn btn-primary">Save changes</button>'
                      +'</div>'
                    +'</div>');

    // populate container list in modal
    var container = []
    var feed = null;
        $.ajax({
          type: "GET",
          url: document.location.origin + '/ccaih/collection_details/RSS',
          dataType: 'xml',
          error: function(){
            console.log('Unable to load feed, Incorrect path or invalid feed');
          },
          success: function(data){
            var feed = $.xml2json(data);
            if (Object.keys(feed.item).length == 1){
              $('#container-selection .modal-body p, #container-selection .modal-body .containers').hide();
            }
            for (var i=0;i<Object.keys(feed.item).length;i++) {
              container.push({ title: feed.item[i].title, desc: feed.item[i].description, link: feed.item[i].link });
            }
            $.each(container, function(i){
              var collectionLink = container[i].link.split('/');
              var containerType = collectionLink[collectionLink.length-1];
              if (containerType == 'repository') {
                $('#container-selection .modal-body').prepend('<h3>Main Terranova Repository</h3>'
                                                          +'<p>If you are unsure which collection you wish to upload to, select this collection.</p>'
                                                          +'<a href="'+container[i].link+'/++add++gu.repository.content.RepositoryItem" title="Main Terranova Repository"><button class="btn btn-success"><i class="icon-white icon-plus-sign"></i> Upload to Terranova Repository</button></a>');
              } else {
              $('#container-selection .modal-body .containers').append('<div class="collection"><h3>'+container[i].title+'</h3>'
                                                          +'<p>'+container[i].desc+'</p>'
                                                          +'<a href="'+container[i].link+'/++add++gu.repository.content.RepositoryItem" title="'+container[i].title+'"><button class="btn btn-success"><i class="icon-white icon-plus-sign"></i> Upload to '+container[i].title+'</button></a></div>');
              }
            });
          }
      });
      
      if ($('#containerTitle').length != 0 && $('#breadcrumbs-current').html().length != 0){
        $('#containerTitle').html('to '+$('#breadcrumbs-current').html());
      }
      
      /* XXX: Broken behavior. Should probably have never been done client-side using JS :/
      var pathArr = document.location.pathname.split('/');
      var view = pathArr[pathArr.length-1];
      if ( $.inArray('view', document.location.pathname.split('/')) != -1 && view == 'view'){
        $('#addItemToCurrent').remove(); 
      } else {
        if ( $('#view-title').length != 0 ){
          $('#view-title').html('Collection');
        }
        $('#searchheader .pull-right').prepend('<a class="btn btn-success" id="addItemToCurrent" href="" title="Add to this collection"><i class="icon-white icon-plus-sign"></i> Add to this collection</a>&nbsp;');
        $('#addItemToCurrent').attr('href', document.location.href+'/++add++gu.repository.content.RepositoryItem');
      }
      */

      //remove crappy bootstrap negative margin from map widget
      $('div.map-widget .row.horizontal').removeClass('row');

      // fix for bad plone temporal date input field
      // no unique markup or classes to attach to, using id searches here
      $('input.date-widget').each(function(){
        if ($(this).attr('id').indexOf('end-day') > 0){
          $(this).before('<br/>');
        } 
      });
      $('input.text-widget').each(function(){
        if ($(this).attr('id').indexOf('starttext') > 0 || $(this).attr('id').indexOf('endtext') > 0){
          $(this).css({'margin-left':'1em', 'width':'60%'});
        }
      });
      
});









